import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Backdrop from '@material-ui/core/Backdrop';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import useLocalStorage from 'hooks/useLocalStorage';

const useStyles = makeStyles(theme => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
  typography: {
    padding: theme.spacing(2),
    color: theme.palette.primary.contrastText,
  },
  grid: {
    padding: theme.spacing(),
    background: theme.palette.primary.main,
  },
}));

interface Props {
  id: string;
  anchorEl: Element;
  text: string;
  onClose: Function;
}

const WhatsNew: React.FC<Props> = ({ anchorEl, text, onClose, id }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const [gotIt, setGotIt] = useLocalStorage(`whats-new-${id}`, false);
  const handleClose = () => {
    setOpen(false);
    onClose();
  };
  if (gotIt) {
    return <></>;
  }
  return (
    <Backdrop className={classes.backdrop} open={open} onClick={handleClose}>
      <Popover
        id={'new-menu'}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <Grid
          container
          justify="flex-end"
          direction="column"
          className={classes.grid}
        >
          <Grid item>
            <Typography className={classes.typography}>What's New</Typography>
            <Divider />
          </Grid>
          <Grid item>
            <Typography className={classes.typography}>{text}</Typography>
          </Grid>
          <Grid item container justify="flex-end">
            <Button variant="contained" onClick={() => setGotIt(true)}>
              GOT IT
            </Button>
          </Grid>
        </Grid>
      </Popover>
    </Backdrop>
  );
};

export default WhatsNew;
