import React, { useState, useRef, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import Button from '@material-ui/core/Button';
import WhatsNewPopover from 'components/popovers/WhatsNew/WhatsNew';

function App() {
  const newRef = useRef<HTMLDivElement | null>(null);
  const [anchorEl, setAnchorEl] = useState();

  useEffect(() => {
    setAnchorEl(newRef.current);
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <div ref={newRef}>
          <Button variant="contained" color="primary">
            New Button
          </Button>
        </div>
        <WhatsNewPopover
          id="new-button"
          text="This button is new, please click it"
          anchorEl={anchorEl}
          onClose={() => {
            setAnchorEl(null);
          }}
        />
      </header>
    </div>
  );
}

export default App;
